package com.beon.androidchallenge.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.domain.model.Fact
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.debounce

@OptIn(FlowPreview::class)
class MainViewModel : ViewModel() {

    private val _currentFact = MutableSharedFlow<Fact?>(
        replay = 0,
        extraBufferCapacity = 1
    )

    val currentFact = _currentFact.debounce(500).asLiveData()

    fun searchNumberFact(number: String) {
        if (number.isEmpty()) {
            _currentFact.tryEmit(null)
            return
        }

        FactRepository.getInstance().getFactForNumber(number, object : FactRepository.FactRepositoryCallback<Fact> {
            override fun onResponse(response: Fact) {
                _currentFact.tryEmit(response)
            }

            override fun onError() {
                
            }

        })
    }
}